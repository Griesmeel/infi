﻿using CSVHelper;
using DataModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CameraAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CameraController : ControllerBase
    {

        [HttpGet]
        public List<Camera> Get()
        {
            List<Camera> cameras = CsvHelper.ReadCameras();
            return cameras;
        }
    }
}
