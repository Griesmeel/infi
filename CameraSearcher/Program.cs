﻿using DataModels;
using CSVHelper;
using System;
using System.Linq;
using System.Collections.Generic;

namespace CameraSearcher
{
    class Program
    {
        static void Main(string[] args)
        {
            // Check if there is one argument
            string query;
            if (args.Length == 1)
            {
                query = args[0];
            }
            else
            {
                Console.WriteLine("Please enter 1 search query:");
                query = Console.ReadLine();
            }

            // List cameras that contain the query
            List<Camera> cameras = CsvHelper.ReadCameras();
            Camera[] result = cameras.Where(i => i.Name.Contains(query)).ToArray();

            if (result.Length == 0)
            {
                Console.WriteLine("There are no search results for this query");
            }

            // Print all the cameras
            foreach (Camera camera in result)
            {
                Console.WriteLine(camera.PrintDetails());
            }

            Console.ReadLine();
        }
    }
}
