﻿// Create OpenStreetMap using Leaflet
var mapUtrecht = L.map('mapid').setView([52.0914, 5.1115], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZ3JpZXNtZWVsIiwiYSI6ImNrMHhta2xjNDA3bWwzY3Fmanh0aDRxd3QifQ.1-Ba0zt6ClviZ_fLq8dq9g'
}).addTo(mapUtrecht);

// Create marker for every camera
for (var i = 0; i < cameras.length; i++) {
    var marker = L.marker([cameras[i].Latitude, cameras[i].Longitude]).addTo(mapUtrecht);
    marker.bindPopup(cameras[i].Name);
}