﻿using System.Collections.Generic;

namespace DataModels
{
    public class IndexModel
    {
        public IndexModel(Dictionary<int, List<Camera>> camerasPerColumn, List<Camera> allCameras)
        {
            this.CamerasPerColumn = camerasPerColumn;
            this.AllCameras = allCameras;
        }

        public Dictionary<int, List<Camera>> CamerasPerColumn { get; set; }
        public List<Camera> AllCameras { get; set; }
    }
}
