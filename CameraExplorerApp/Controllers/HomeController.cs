﻿using System.Configuration;
using DataModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CameraExplorerApp.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> IndexAsync()
        {         
            // Get all cameras from API
            string apiUri = ConfigurationManager.AppSettings.Get("apiUri");
            List<Camera> cameras = await DoCallAsync(apiUri);

            // Group cameras based on their number
            Dictionary<int, List<Camera>> camerasPerColumn = cameras.GroupBy(camera => GetColumnIndex(camera.Number))
                .ToDictionary(index => index.Key, cameras => cameras.ToList());

            IndexModel viewModel = new(camerasPerColumn, cameras);
            return View(viewModel);
        }

        private static async Task<List<Camera>> DoCallAsync(string uri)
        {           
            using HttpClient httpClient = new();
            HttpResponseMessage response = await httpClient.GetAsync(uri);
            string camerasString = await response.Content.ReadAsStringAsync();

            List<Camera> cameras = JsonConvert.DeserializeObject<List<Camera>> (camerasString);
            return cameras;
        }

        private static int GetColumnIndex (int cameraNumber)
        {
            if (cameraNumber % 3 == 0 & cameraNumber % 5 == 0)
            {
                return 3;
            } 
            else if (cameraNumber % 3 == 0)
            {
                return 1;
            }
            else if (cameraNumber % 5 == 0)
            {
                return 2;
            }
            else
            {
                return 4;
            }
        }
    }
}
