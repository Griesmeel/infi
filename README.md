# Programmeren voor Infi

Deze repository bevat mijn implementatie van de volgende opdracht: https://github.com/infi-nl/everybody-codes. 

- Gebruik CameraSearcher.bat om het CLI programma te runnen.
- Gebruik de solution om de API en web app te runnen.

![Camera Explorer](Screenshot.png)