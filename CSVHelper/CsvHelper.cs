﻿using DataModels;
using System;
using System.Collections.Generic;
using System.IO;

namespace CSVHelper
{
    public static class CsvHelper
    {
        public static List<Camera> ReadCameras()
        {
            string fileName = "cameras-defb.csv";
            string filePath = Path.Combine(Environment.CurrentDirectory, @"..\data\", fileName);

            List<Camera> cameras = new();

            string[] lines = File.ReadAllLines(filePath);

            // Loop over lines, skip header
            for (int line = 1; line < lines.Length; line++)
            {
                string[] values = lines[line].Split(';');

                // Skip lines that do not contain three values
                if (values.Length == 3)
                {
                    // Extract camera number from first column
                    int cameraNumber = Int16.Parse(values[0].Substring(7, 3));

                    Camera camera = new(cameraNumber, values[0], values[1], values[2]);
                    cameras.Add(camera);
                }
            }

            return cameras;
        }
    }
}
