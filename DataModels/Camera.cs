﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModels
{
    public class Camera
    {
        public Camera(int number, string name, string latitude, string longitude)
        {
            this.Number = number;
            this.Name = name;
            this.Latitude = latitude;
            this.Longitude = longitude;
        }

        public int Number { get; set; }
        public string Name { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public string PrintDetails()
        {
            string pretty_line = $"{this.Number} | {this.Name} | {this.Latitude} | {this.Longitude}";
            return pretty_line;
        }
    }
}
